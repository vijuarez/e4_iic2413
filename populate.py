from db_functions import DB
import json
import subprocess

def load_to_mongo(dbname, collection, filename):
    """
    Imports the json file with the location specified in 'filename' to the local mongodb server
    """
    args = ["mongoimport", "--db", dbname, "--collection", collection, "--drop", "--file", filename, "--jsonArray"]
    status = subprocess.check_output(args, universal_newlines = True)
    print(status)
    
def extract_users():
    pass

def id_generator():
    """
    Generator that manages the new message id, instead of using the mongoDB ObjectId.
    """
    num = 1
    while True:
        yield num
        num += 1

def add_msg_id():
    complete = []
    with open("data/messages.json", "r", encoding='utf8') as file:
        data = json.load(file)
    for elemento in data:
        elemento['message_id'] = next(message_id)
    with open("data/messages_with_id.json", "w", encoding='utf8') as newfile:
        json.dump(data, newfile, indent=2, ensure_ascii=False)

def csv_to_json():
    '''
    The initial idea was to use psycopg2 to transfer data from the PostgreSQL, but the server provided doesn't support
    the installation through the pip command, and i'm really afraid of breaking things so according to this
    (https://github.com/IIC2413/Syllabus-2018-2/issues/147) issue, we'll make it by reading the Users csv file.
    '''
    with open("data/Usuarios.csv", "r", encoding="utf-8-sig") as file:
        users = []
        file.readline() # This is the initial line with column names
        for line in file:
            uid, surname, name, email, gender, age, password = line.strip().split(",")
            uid = int(uid)
            if age != "No Aplica":
                age = int(age)
            # Showing users passwords on an API is kind of weird, so we are not showing that ;3
            user_dict = {"uid": uid, "name": name, "surname": surname, "email": email, "gender": gender, "age": age}
            users.append(user_dict)
    with open("data/Users.json", "w", encoding='utf8') as file:
        json.dump(users, file, indent=2, ensure_ascii=False)   
            

if __name__ == '__main__':
    message_id = id_generator()
    add_msg_id()
    csv_to_json()
    load_to_mongo("E4", "messages", "data/messages_with_id.json")
    #extract_users()
    load_to_mongo("E4", "users", "data/Users.json")
    