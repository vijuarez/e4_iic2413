# Entrega 4
> Look on my works, ye Mighty, and despair!

_Realización, un mes más tarde, cuando nunca nadie vuelva a correr este código._

## Introducción

Esta carpeta corresponde a la entrega 4 del grupo 4 + 19. Consiste en 3 archivos importantes:

* `app.py`: El servidor de Flask en si.
* `db_functions.py`: Las funciones encargadas de monitorear la aplicación y conectar con MongoDB.
* `populate.py`: Script encargado de poblar la base de datos.

## Dependencias

* Python 3.6.6
* PyMongo 3.7.2
* Flask 1.0.2
* Requests 2.20

### IP Stack API

La aplicación está preparada para calcular correctamente la posición geo-espacial desde donde se envian los mensajes, con ayuda de esta API. Sin embargo, hay que tener en consideración lo siguiente:

* Debe existir un archivo `env` que contenga los datos `API_KEY=<Llave de la API>`, la cual se incluye junto con la entrega.

## Corriendo la aplicación

Se espera que exista el archivo `data/messages.json` para cargar los datos a MongoDB y el archivo `data/Usuarios.csv` para cargar los datos de los usuarios. Los datos para conectarse a MongoDB se importan desde `db_function.py`. Es necesario que se inicie el servicio de mongoDB (en caso de ser necesario) previo a la ejecución de `populate.py`, debido a que de otra forma el script no podrá ingresar los datos a MongoDB. Teniendo en consideración lo anterior, ejecutar `populate.py` para crear las bases de datos de usuarios y mensajes en el MongoDB local.

La aplicación puede lanzarse fácilmente con el comando:

```bash
python3 -m flask run
```

Esto hará funcionar la aplicación de forma local en el puerto 5000.

El archivo `db_function.py` contiene constantes con la información para conectarse a MongoDB.


## Consideraciones

* Al ejecutar `populate.py`, la funcion `add_msg_id()` agrega el atributo `"message_id"` a cada uno de los mensajes de `data/messages.json`. El nuevo archivo JSON se guarda en `data/messages_with_id.json` para posteriormente importar los datos al mongoDB local. En cada una de las consultas a la API, el ID del mensaje se considera como el `"message_id"` asociado a cada mensaje.


## Rutas

* `/messages/<int:item_id>`: Dada cierta id interna, retorna los datos del mensaje con esa id.
    + GET <localhost:5000/messages/1> entrega la información del mensaje con id 1.

* `/users/<int:item_id>`: Dada cierta id, retorna los datos de ese usuario.
    + GET <localhost:5000/users/12> entrega la información del usuario con id 12 y todos sus mensajes emitidos.

* `/messages/exchange?id1=<int>&id2=<int>`: Dados dos ids pertenecientes a dos usuarios, entrega una lista con los mensajes intercambiados entre esos dos usuarios.
    + GET <localhost:5000/messages/exchange?id1=1&id2=32> entrega la información de todos los mensajes intercambiados por los usuarios con id 1 y 32.

* `/messages/search?id=<int, optional>&must_have=<str>,<str>&could_have=<str>,<str>&cant_have=<str>,<str>`: Dado un id de usuario opcional, una lista de 0..* strings separados por coma que el mensaje debe tener, una lista 0..* strings separados por coma que el mensaje puede tener y una lista de 0..* strings separados por coma que el mensaje no puede tener. Retorna una lista con los mensajes que cumplan los requisitos.
    + GET <localhost:5000/messages/search?uid=19&must_have=Te&cant_have=despido> Entrega la informacion de todos los mensajes enviados por el usuario con ID 19, que tienen la palabra "Te", y no contienen a "despido".
    + GET <localhost:5000/messages/search?must_have=cuento&cant_have=churrasco> Entrega la información de todos los mensjes que tienen la palabra "cuento" y no tienen a la palabra "churrasco".
    + GET <localhost:5000/messages/search?uid=19&must_have=Te&cant_have=despido,hablar> Entrega la información de todos los mensajes enviados por el usuario con ID 19, que tienen la palabra "Te" y no tienen a las palabras "despido" y "hablar".


* `/messages/send?id1=<int>&id2=<int>&message=<str>`: Dado dos id de usuario y una string, manda un mensaje desde id1 a id2 con los contenidos de message. Como se explicó anteriormente, los datos de localización se obtienen mediante el uso de una API.
    * POST <localhost:5000/messages/send?id1=12&id2=43&message=Hola> manda el mensaje "Hola" desde el usuario con ID 12 al usuario con ID 43. Si no hay problemas, entrega un json con el id del mensaje nuevo.

* `/messages/int:<item_id>`: Dado un id interno de un mensaje, lo borra de la base de datos.
    * DELETE <localhost:5000/messages/1> borra al mensaje con el ID 1 de la base de datos.

Para cualquier ruta, en caso de error (no encontrar información), siempre retorna el siguiente JSON:

`{'success': 0}`
