from db_functions import (message_info, user_info, user_messages, exchanged_messages,
                          search_message, search_message_user, add_message, remove_message)
from flask import Flask, jsonify, request
import requests

app = Flask(__name__)

with app.app_context():
    SUCCESS = jsonify({"status": 1})
    FAILURE = jsonify({"status": 0})

try:
    with open('env') as file:
        API_KEY = file.readline().strip().split('=')[1]
except FileNotFoundError:
    API_KEY = None


def check_value(value):
    if value:
        if isinstance(value, list):
            return jsonify({'data': value}), 200
        else:
            return jsonify(value), 200
    else:
        return FAILURE, 404


def get_position():
    if API_KEY:
        url = 'http://api.ipstack.com/'
        ip = requests.get('https://api.ipify.org').text
        data = requests.get(url=url + ip, params={'access_key': API_KEY}).json()
        return float(data['latitude']), float(data['longitude'])
    else:
        return 0.0, 0.0


@app.route('/')
def index():
    return SUCCESS


@app.route('/messages/<int:item_id>', methods=['GET'])
def get_message(item_id):   
    return check_value(message_info(item_id))


@app.route('/users/<int:item_id>', methods=['GET'])
def get_user(item_id):
    info = user_info(item_id)
    msg = user_messages(item_id)
    answer = dict()
    answer['info'] = info if info else {"status": 0}
    answer['messages'] = msg if msg else {"status": 0}
    return jsonify(answer)


@app.route('/messages/exchange', methods=['GET'])
def get_exchange():
    id1 = int(request.args.get('id1'))
    id2 = int(request.args.get('id2'))
    return check_value(exchanged_messages(id1, id2))


@app.route('/messages/search', methods=['GET'])
def search_msg():
    must_have = request.args.get('must_have')
    could_have = request.args.get('could_have')
    cant_have = request.args.get('cant_have')
    must_have = must_have.split(',') if must_have else []
    could_have = could_have.split(',') if could_have else []
    cant_have = cant_have.split(',') if cant_have else []
    item_id = request.args.get('uid')
    if not item_id:
        return check_value(search_message(must_have, could_have, cant_have))
    else:
        return check_value(search_message_user(item_id, must_have, could_have, cant_have))


@app.route('/messages/send', methods=['POST'])
def send_message():
    id1 = int(request.args.get('id1'))
    id2 = int(request.args.get('id2'))
    message = request.args.get('message')
    lat, lon = get_position()
    return check_value(add_message(id1, id2, message, lat, lon))


@app.route('/messages/<int:item_id>', methods=['DELETE'])
def delete_message(item_id):
    return check_value(remove_message(item_id))


if __name__ == '__main__':
    app.run(port=5000)
