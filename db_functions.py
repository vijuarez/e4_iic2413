import pymongo
from bson.objectid import ObjectId

from typing import Dict, Any, List, Callable
from functools import wraps
from itertools import chain
import logging

import datetime


# Singletons

logger = logging.getLogger('db_functions_log')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('db_functions.log')
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)

HOST = 'localhost'
PORT = 27017

CLIENT = pymongo.MongoClient(HOST, PORT)

DB_NAME = 'E4'

DB = CLIENT[DB_NAME]


# Definitions


@wraps
def monitoring(func: Callable[[Any], Any]) -> Callable[[Any], Any]:
    """
    Wrapper for logging, stores in a log the execution data of any wrapped function.

    Uses global variable logger to log the data.
    """
    def logged_func(*args, **kwargs):
        logger.info(f'called function {f.__name__} with arguments {", ".join(map(repr, chain(args, kwargs.items())))}.')
        return func(*args, **kwargs)
    return logged_func

def sanitize_id(items, name='_id'):
    """
    This exists because ObjectID objects are not serializable
    """
    for i in items:
        i[name] = str(i[name])


@monitoring
def message_info(item_id: int) -> Dict[Any, Any]:
    """
    Receives the internal id of a message, return JSON information of that message.

    On failure, ie no message found, returns empty JSON and logs the incident.
    """
    msg = DB.messages
    result = msg.find_one({'message_id': item_id})
    if result:
        result['_id'] = str(result['_id'])
        logger.info(f'success on message_info')
        return result
    else:
        logger.debug(f'error on message_info, returning empty dict')
        return {}


@monitoring
def user_info(item_id: int) -> Dict[Any, Any]:
    """
    Receives the id of an user, return JSON information of that user.

    On failure, ie no user found, returns empty JSON and logs the incident.
    """
    usr = DB.users
    result = usr.find_one({'uid': item_id})
    if result:
        result['_id'] = str(result['_id'])
        logger.info(f'success on user_info')
        return result
    else:
        logger.debug(f'error on user_info, returning empty dict')
        return {}


@monitoring
def user_messages(item_id: int) -> List[Dict[Any, Any]]:
    """
    Receives the id of a user, return JSON list with messages received by said user.

    On failure, ie no messages or user found, returns empty JSON and logs the incident.
    """
    msg = DB.messages
    result = list(msg.find({'$or': [{'receptant': item_id}, {'sender': item_id}])
    if result:
        logger.info(f'success on user_messages')
        sanitize_id(result)
        return result
    else:
        logger.debug(f'error on user_messages, returning empty list')
        return []


@monitoring
def exchanged_messages(id1: int, id2: int) -> List[Dict[Any, Any]]:
    """
    Receives the id of two users, return JSON list with messages exchanged by those users.

    On failure, ie no messages found, returns empty JSON list and logs the incident.
    """
    msg = DB.messages
    result = list(msg.find({'$and': [{'$or': [{'receptant': id1}, {'receptant': id2}]},
                                     {'$or': [{'sender': id1}, {'sender': id2}]}]}))
    if result:
        sanitize_id(result)
        logger.info(f'success on exchanged_messages')
        return result
    else:
        logger.debug(f'error on exchanged_messages, returning empty list')
        return []


@monitoring
def search_message(must_be: List[str]=None, could_be: List[str]=None, cant_be: List[str]=None) -> List[Dict[Any, Any]]:
    """
    Receives 3 lists with strings. Returns messages that obey the following rules:

        i) Don't contain any member of cant_be as substring.
        ii) Have all members of must_be as substrings (or must_be is empty)

    And, if must_be is empty:

        iii) Contain at least one member of could_be as substring

    On failure, ie no messages found, returns empty JSON list and logs the incident.
    """
    must_be = must_be if must_be else list()
    could_be = could_be if could_be else list()
    cant_be = cant_be if cant_be else list()
    msg = DB.messages
    result = msg.find()
    filtered = []
    for msg in result:
        logger.info(f'analyzing {repr(msg["message"])} in search_message')
        if must_be:
            if all(msg['message'].lower().find(i.lower()) != -1 for i in must_be):
                if not any(msg['message'].lower().find(i.lower()) != -1 for i in cant_be):
                    filtered.append(msg)
        else:
            if any(msg['message'].lower().find(i.lower()) != -1 for i in could_be):
                if not any(msg['message'].lower().find(i.lower()) != -1 for i in cant_be):
                    filtered.append(msg)
    if filtered:
        logger.info(f'success on search_message')
        sanitize_id(filtered)
        return filtered
    else:
        logger.debug(f'error on search_message, returning empty list')
        return []


@monitoring
def search_message_user(item_id: int, must_be: List[str]=None, could_be: List[str]=None,
                        cant_be: List[str]=None) -> List[Dict[Any, Any]]:
    """
    Follows the same rules as search_message, but only works with messages sent by the user
    whose id is given.
    """
    must_be = must_be if must_be else list()
    could_be = could_be if could_be else list()
    cant_be = cant_be if cant_be else list()
    msg = DB.messages
    result = msg.find({'sender': int(item_id)})
    filtered = []
    for msg in result:
        logger.info(f'analyzing {repr(msg["message"])} in search_message_user')
        if must_be:
            if all(msg['message'].lower().find(i.lower()) != -1 for i in must_be):
                if not any(msg['message'].lower().find(i.lower()) != -1 for i in cant_be):
                    filtered.append(msg)
        else:
            if any(msg['message'].lower().find(i.lower()) != -1 for i in could_be):
                if not any(msg['message'].lower().find(i.lower()) != -1 for i in cant_be):
                    filtered.append(msg)
    if filtered:
        logger.info(f'success on search_message_user')
        sanitize_id(filtered)
        return filtered
    else:
        logger.debug(f'error on search_message_user, returning empty list')
        return []


@monitoring
def add_message(from_id: int, to_id: int, message: str, lat: float, lon: float) -> bool:
    """
    Receives data to create a new message. The author id in from_id, the recipient id in
    to_id, the message itself and the latitude/longitude coordinates. Returns True on 
    success, returns False and logs the incident in case of failure.
    """
    max_id = int(DB.messages.find().sort("message_id",pymongo.DESCENDING).limit(1)[0]["message_id"])
    msg = {"message": message,
           "sender": from_id, 
           "receptant": to_id,
           "lat": lat, 
           "long": lon,
           "date": datetime.date.today().strftime('%Y-%m-%d'),
           "message_id": max_id + 1}
    result = DB.messages.insert_one(msg)
    if result:
        logger.info(f'success on add_message')
        return {"status": "success", "new_message_id": max_id + 1}
    else:
        logger.debug(f'error on add_message, returning false')
        return False


@monitoring
def remove_message(item_id: int) -> bool:
    """
    Given an internal id of a message, deletes said message from the database. In success, it
    returns True. If not, it return False and logs the incident.
    """
    msg = DB.messages
    result = msg.delete_one({"message_id": item_id})
    if result.deleted_count != 0:
        logger.info(f'success on remove_message')
        return {"status": "success"}
    else:
        logger.debug(f'error on remove_message, returning false')
        return False
